The files in this folder are used when building the Windows version of this package
and contains the include file and dll of `libusb` release v1.0.21
obtained from this [repository](https://github.com/libusb/libusb/releases).

