// Package airspy implement a Go binding to the libairspy C library.
package airspy

// #include <stdlib.h>
// #include "airspy.h"
//
// // Declaration of C callback function defined in "cfuncs.go"
// int cCallback(airspy_transfer* t);
import "C"

import (
	"errors"
	"strings"
	"unsafe"
)

// Error codes
const (
	ErrorInvalidParam     = C.AIRSPY_ERROR_INVALID_PARAM
	ErrorNotFound         = C.AIRSPY_ERROR_NOT_FOUND
	ErrorBusy             = C.AIRSPY_ERROR_BUSY
	ErrorNoMem            = C.AIRSPY_ERROR_NO_MEM
	ErrorLibusb           = C.AIRSPY_ERROR_LIBUSB
	ErrorThread           = C.AIRSPY_ERROR_THREAD
	ErrorStreamingThread  = C.AIRSPY_ERROR_STREAMING_THREAD_ERR
	ErrorStreamingStopped = C.AIRSPY_ERROR_STREAMING_STOPPED
	ErrorOther            = C.AIRSPY_ERROR_OTHER
)

// SampleType is type for sample
type SampleType uint32

const (
	// SampleFloat32IQ is a sample composed of 2 * float32 (I and Q)
	SampleFloat32IQ SampleType = C.AIRSPY_SAMPLE_FLOAT32_IQ
	// SampleFloat32Real is a sample composed of 1 * float32 (real value)
	SampleFloat32Real SampleType = C.AIRSPY_SAMPLE_FLOAT32_REAL
	// SampleInt16IQ is a sample composed of 2 * int16 (I and Q)
	SampleInt16IQ SampleType = C.AIRSPY_SAMPLE_INT16_IQ
	// SampleInt16Real is a sample composed of 1 * int16 (real value)
	SampleInt16Real SampleType = C.AIRSPY_SAMPLE_INT16_REAL
	// SampleUint16Real is a sample composed of 1 * uint16 (real value)
	SampleUint16Real SampleType = C.AIRSPY_SAMPLE_UINT16_REAL
	// SampleRaw is a raw sample
	SampleRaw SampleType = C.AIRSPY_SAMPLE_RAW
	// SampleEnd is a marker of the end of sample types
	SampleEnd SampleType = C.AIRSPY_SAMPLE_END
)

// Device describes an Airspy device
type Device struct {
	cdev *C.struct_airspy_device
	cb   func(t *Transfer) int
}

// LibVersionInfo describes the version of the airspy library
type LibVersionInfo struct {
	MajorVersion uint32
	MinorVersion uint32
	Revision     uint32
}

// ReadPartidSerialno describes the part id and serial number of a device
type ReadPartidSerialno struct {
	Partid   [2]uint32
	Serialno [4]uint32
}

// Transfer is the argument of the callback function called with received data
// from the device.
type Transfer struct {
	Dev            *Device
	Samples        unsafe.Pointer
	SampleCount    int
	DroppedSamples uint64
	SType          SampleType
}

// Map of device C pointers to Go Device pointer
// Used by callback functions
var mapDevices = map[*C.struct_airspy_device]*Device{}

// LibVersion reads the airspy library version information
func LibVersion(libver *LibVersionInfo) {

	var ver C.airspy_lib_version_t
	C.airspy_lib_version(&ver)
	libver.MajorVersion = uint32(ver.major_version)
	libver.MinorVersion = uint32(ver.minor_version)
	libver.Revision = uint32(ver.revision)
}

// OpenSn opens a device specified by its serial number
func OpenSn(dev *Device, serialNumber uint64) error {

	res := C.airspy_open_sn(&dev.cdev, C.uint64_t(serialNumber))
	if res != C.AIRSPY_SUCCESS {
		return errorName(res)
	}
	mapDevices[dev.cdev] = dev
	return nil
}

// Open opens the next available device
func Open(dev *Device) error {

	res := C.airspy_open(&dev.cdev)
	if res != C.AIRSPY_SUCCESS {
		return errorName(res)
	}
	mapDevices[dev.cdev] = dev
	return nil
}

// Close closes a previously opened device
func Close(dev *Device) error {

	res := C.airspy_close(dev.cdev)
	if res != C.AIRSPY_SUCCESS {
		return errorName(res)
	}
	mapDevices[dev.cdev] = nil
	return nil
}

// SampleRates returns a list of the device supported sample rates
func SampleRates(dev *Device) ([]uint32, error) {

	// Get number of sample rates
	var count C.uint32_t
	res := C.airspy_get_samplerates(dev.cdev, &count, 0)
	if res != C.AIRSPY_SUCCESS {
		return nil, errorName(res)
	}

	// Get sample rates
	rates := make([]uint32, count)
	res = C.airspy_get_samplerates(dev.cdev, (*C.uint32_t)(&rates[0]), count)
	return rates, nil
}

// SetSampleRate sets the device sample rate
// rate can be the value in Hz or the index of the rate in the list returned by SampleRates()
func SetSampleRate(dev *Device, rate uint32) error {

	res := C.airspy_set_samplerate(dev.cdev, C.uint32_t(rate))
	if res != C.AIRSPY_SUCCESS {
		return errorName(res)
	}
	return nil
}

// SetConversionFilterFloat32 sets the IQ conversion filter coefficients for sample type: SampleFloat32IQ
func SetConversionFilterFloat32(dev *Device, filter []float32) error {

	res := C.airspy_set_conversion_filter_float32(dev.cdev, (*C.float)(&filter[0]), C.uint32_t(len(filter)))
	if res != C.AIRSPY_SUCCESS {
		return errorName(res)
	}
	return nil
}

// SetConversionFilterInt16 sets the IQ conversion filter coefficients for sample type: SampleInt16IQ
func SetConversionFilterInt16(dev *Device, filter []int16) error {

	res := C.airspy_set_conversion_filter_int16(dev.cdev, (*C.int16_t)(&filter[0]), C.uint32_t(len(filter)))
	if res != C.AIRSPY_SUCCESS {
		return errorName(res)
	}
	return nil
}

// StartRx starts the device reception calling the specified callback periodically
// with transfer data information.
// The specified callback is executed in another goroutine
// To stop the reception use StopRx()
func StartRx(dev *Device, cb func(t *Transfer) int) error {

	res := C.airspy_start_rx(dev.cdev, C.airspy_sample_block_cb_fn(C.cCallback), nil)
	if res != C.AIRSPY_SUCCESS {
		return errorName(res)
	}
	dev.cb = cb
	return nil
}

// goCallback is a callback called by 'cCallback' defined in "cfuncs.go"
// when data from the device is received
//export goCallback
func goCallback(ct *C.airspy_transfer) int {

	// Get the pointer of Go device for this transfer
	dev := mapDevices[ct.device]
	if dev == nil {
		panic("goCallback: device not found")
	}

	// Build Go transfer struct and calls user go callback
	var t Transfer
	t.Dev = dev
	t.Samples = ct.samples
	t.SampleCount = int(ct.sample_count)
	t.DroppedSamples = uint64(ct.dropped_samples)
	t.SType = SampleType(ct.sample_type)
	return dev.cb(&t)
}

// StopRx stops the device reception
func StopRx(dev *Device) error {

	res := C.airspy_stop_rx(dev.cdev)
	if res != C.AIRSPY_SUCCESS {
		return errorName(res)
	}
	return nil
}

// IsStreaming returns if the device is transferring data or not
func IsStreaming(dev *Device) bool {

	res := C.airspy_is_streaming(dev.cdev)
	if res == 0 {
		return false
	}
	return true
}

// BoardIDRead reads and returns the device board id
func BoardIDRead(dev *Device) (uint8, error) {

	var value uint8
	res := C.airspy_board_id_read(dev.cdev, (*C.uint8_t)(&value))
	if res != C.AIRSPY_SUCCESS {
		return 0, errorName(res)
	}
	return value, nil
}

// VersionStringRead reads and returns the device firmware version string
func VersionStringRead(dev *Device) (string, error) {

	const maxSize = 128
	version := C.CString(strings.Repeat(" ", maxSize))
	defer C.free(unsafe.Pointer(version))
	res := C.airspy_version_string_read(dev.cdev, version, maxSize)
	if res != C.AIRSPY_SUCCESS {
		return "", errorName(res)
	}
	return C.GoString(version), nil
}

// BoardPartidSerialnoRead reads the device part id and serial number
func BoardPartidSerialnoRead(dev *Device, partid *ReadPartidSerialno) error {

	var cpartid C.airspy_read_partid_serialno_t
	res := C.airspy_board_partid_serialno_read(dev.cdev, &cpartid)
	if res != C.AIRSPY_SUCCESS {
		return errorName(res)
	}
	partid.Partid[0] = uint32(cpartid.part_id[0])
	partid.Partid[1] = uint32(cpartid.part_id[1])
	for i := 0; i <= 3; i++ {
		partid.Serialno[i] = uint32(cpartid.serial_no[i])
	}
	return nil
}

// SetSampleType sets the type for device samples read during reception
func SetSampleType(dev *Device, sampleType SampleType) error {

	res := C.airspy_set_sample_type(dev.cdev, uint32(sampleType))
	if res != C.AIRSPY_SUCCESS {
		return errorName(res)
	}
	return nil
}

// SetFreq sets the device frequency in Hz from 24000000 (24Mhz) to 1750000000 (1.75GHz)
func SetFreq(dev *Device, freqHz uint32) error {

	res := C.airspy_set_freq(dev.cdev, C.uint32_t(freqHz))
	if res != C.AIRSPY_SUCCESS {
		return errorName(res)
	}
	return nil
}

// SetLnaGain sets the device LNA gain from 0 to 15
func SetLnaGain(dev *Device, value uint8) error {

	res := C.airspy_set_lna_gain(dev.cdev, C.uint8_t(value))
	if res != C.AIRSPY_SUCCESS {
		return errorName(res)
	}
	return nil
}

// SetMixerGain sets the device mixer gain from 0 to 15
func SetMixerGain(dev *Device, value uint8) error {

	res := C.airspy_set_mixer_gain(dev.cdev, C.uint8_t(value))
	if res != C.AIRSPY_SUCCESS {
		return errorName(res)
	}
	return nil
}

// SetVgaGain sets the device VGA gain
func SetVgaGain(dev *Device, value uint8) error {

	res := C.airspy_set_vga_gain(dev.cdev, C.uint8_t(value))
	if res != C.AIRSPY_SUCCESS {
		return errorName(res)
	}
	return nil
}

// SetLnaAgc enables/disables the LNA Automatic Gain Control
func SetLnaAgc(dev *Device, enable bool) error {

	value := C.uint8_t(0)
	if enable {
		value = 1
	}
	res := C.airspy_set_lna_agc(dev.cdev, value)
	if res != C.AIRSPY_SUCCESS {
		return errorName(res)
	}
	return nil
}

// SetMixerAgc enables/disables the MIXER Automatic Gain Control
func SetMixerAgc(dev *Device, enable bool) error {

	value := C.uint8_t(0)
	if enable {
		value = 1
	}
	res := C.airspy_set_mixer_agc(dev.cdev, value)
	if res != C.AIRSPY_SUCCESS {
		return errorName(res)
	}
	return nil
}

// SetLinearityGain sets the linearity gain from 0 to 21
func SetLinearityGain(dev *Device, value uint8) error {

	res := C.airspy_set_linearity_gain(dev.cdev, C.uint8_t(value))
	if res != C.AIRSPY_SUCCESS {
		return errorName(res)
	}
	return nil
}

// SetSensitivityGain sets the sensitivy gain from 0 to 21
func SetSensitivityGain(dev *Device, value uint8) error {

	res := C.airspy_set_sensitivity_gain(dev.cdev, C.uint8_t(value))
	if res != C.AIRSPY_SUCCESS {
		return errorName(res)
	}
	return nil
}

// SetRfBias enables or disables RF Bias
func SetRfBias(dev *Device, enable bool) error {

	value := C.uint8_t(0)
	if enable {
		value = 1
	}
	res := C.airspy_set_rf_bias(dev.cdev, value)
	if res != C.AIRSPY_SUCCESS {
		return errorName(res)
	}
	return nil
}

// SetPacking enable or disables packing
func SetPacking(dev *Device, enable bool) error {

	value := C.uint8_t(0)
	if enable {
		value = 1
	}
	res := C.airspy_set_packing(dev.cdev, value)
	if res != C.AIRSPY_SUCCESS {
		return errorName(res)
	}
	return nil
}

// BoardIDName returns the board name from its specified id
func BoardIDName(boardID uint8) string {

	cstr := C.airspy_board_id_name(uint32(boardID))
	return C.GoString(cstr)
}

// errorName returns an error with the string associated with the specified error code
func errorName(errcode C.int) error {

	cstr := C.airspy_error_name(int32(errcode))
	return errors.New(C.GoString(cstr))
}
