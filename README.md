# GoAirspy - Go bindings for libairspy

This package implements a Go binding for [libairspy](https://bitbucket.org/airspy/airspyone_host) a C library
which allows access to the [AirSpy](http://airspy.com) Sofware Defined Radios through the USB interface.
The libairspy C sources are included in this package.

## Dependencies for Linux installation

* Install your distribution's `libusb-dev` package.
* GCC toolchain.

## Dependencies for Windows installation

* For your convenience `libusb.h` and `libusb-1.0.dll` from
  the `libusb` release [repository](https://bitbucket.org/libusb/libusb/releases)
  were copied in this package `win64` folder.
  You need to copy `libusb-1.0.dll` to a folder which is listed in your
  Windows system PATH (such as `$GOPATH/bin`).
* [Mingw-W64](https://mingw-w64.org) toolchain (We used version: `x86_64-7.1.0-release-posix-seh-rt_v5-rev2.7`).

## Installation

After the dependencies are installed this Go package can be installed by:

`>go get bitbucket.org/leonsal/goairspy`

A test program [goairspy_test](https://bitbucket.org/leonsal/goairspy/tree/master/goairspy_test)
exercises most functions of this package. To install it:

`>go get bitbucket.org/leonsal/goairspy/goairspy_test`

For Linux you may need to copy `airspyone_host/airspy_tools/52-airspy.rules` to
your `udev` rules directory (`/etc/udev/rules.d`).

