package main

import (
	"flag"
	"fmt"
	"os"
	"time"

	"bitbucket.org/leonsal/goairspy"
)

const (
	MaxDevice = 32
	ProgName  = "GoAirspy Test"
	ExecName  = "goairspy_test"
	Vmajor    = 0
	Vminor    = 1
)

// Command line options
var (
	oSrate     = flag.Uint("srate", 10000000, "Sets the sample rate in MSPS")
	oStype     = flag.String("stype", "F32IQ", "Sets the sample type (F32IQ|F32REAL|I16IQ|I16REAL|U16REAL|RAW")
	oFreq      = flag.Uint("freq", 89000000, "Sets the tuner frequency in Hz")
	oLnaGain   = flag.Uint("lnagain", 5, "Sets the LNA gain (0..15)")
	oMixerGain = flag.Uint("mixergain", 10, "Sets the LNA gain (0..15)")
	oVgaGain   = flag.Uint("vgagain", 10, "Sets the VGA gain (0..15)")
	oLinGain   = flag.Uint("lingain", 10, "Sets the linearity gain (0..21)")
	oSensGain  = flag.Uint("sensgain", 10, "Sets the sensitivity gain (0..21)")
	oLnaAgc    = flag.Bool("lnaagc", false, "Enable/disable LNA AGC")
	oMixerAgc  = flag.Bool("mixeragc", false, "Enable/disable Mixer AGC")
	oRfBias    = flag.Bool("rfbias", false, "Enable/disable RF Bias")
	oPacking   = flag.Bool("packing", false, "Enable/disable packing")
	oRx        = flag.Bool("rx", false, "Enable/disable reception")
)

// SampleType names
var mapStype = map[string]airspy.SampleType{
	"F32IQ":   airspy.SampleFloat32IQ,
	"F32REAL": airspy.SampleFloat32Real,
	"I16IQ":   airspy.SampleInt16IQ,
	"I16REAL": airspy.SampleInt16Real,
	"U16REAL": airspy.SampleUint16Real,
	"RAW":     airspy.SampleRaw,
}

func main() {

	// Parse command line parameters
	flag.Usage = usage
	flag.Parse()

	// Open devices
	devices := make([]*airspy.Device, 0)
	for i := 0; i < MaxDevice; i++ {
		var dev airspy.Device
		err := airspy.Open(&dev)
		if err != nil {
			break
		}
		devices = append(devices, &dev)
	}
	fmt.Printf("%25s: %d\n", "Found Airspy devices", len(devices))
	if len(devices) == 0 {
		return
	}

	showInfo(devices[0])
	setParams(devices[0])
	if *oRx {
		startRx(devices[0])
	}

	err := airspy.Close(devices[0])
	if err != nil {
		panic(err)
	}
}

func showInfo(dev *airspy.Device) {

	// Show library version
	var version airspy.LibVersionInfo
	airspy.LibVersion(&version)
	fmt.Printf("%25s: %d.%d.%d\n",
		"Airspy Lib Version", version.MajorVersion, version.MinorVersion, version.Revision)

	// Show board id and name
	bid, err := airspy.BoardIDRead(dev)
	if err != nil {
		panic(err)
	}
	bname := airspy.BoardIDName(bid)
	fmt.Printf("%25s: %d (%s)\n", "Board ID Number", bid, bname)

	// Show firmware version
	ver, err := airspy.VersionStringRead(dev)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%25s: %s\n", "Firmware Version", ver)

	// Show part id and serial number
	var partid airspy.ReadPartidSerialno
	err = airspy.BoardPartidSerialnoRead(dev, &partid)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%25s: 0x%08X 0x%08X\n", "Part ID Number", partid.Partid[0], partid.Partid[1])
	fmt.Printf("%25s: 0x%08X0x%08X\n", "Serial Number", partid.Serialno[2], partid.Serialno[3])

	// Show supported sample rates
	rates, err := airspy.SampleRates(dev)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%25s:", "Supported sample rates")
	for i := 0; i < len(rates); i++ {
		if i == 0 {
			fmt.Printf(" %d MSPS\n", rates[i])
			continue
		}
		fmt.Printf("%25s  %d MSPS\n", "", rates[i])
	}
}

func setParams(dev *airspy.Device) {

	err := airspy.SetSampleRate(dev, uint32(*oSrate))
	if err != nil {
		panic(err)
	}
	fmt.Printf("%25s: %d\n", "SetSampleRate", *oSrate)

	stype, ok := mapStype[*oStype]
	if !ok {
		panic("Invalid SampleType")
	}
	err = airspy.SetSampleType(dev, stype)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%25s: %v (%v)\n", "SetSampleType", *oStype, stype)

	err = airspy.SetFreq(dev, uint32(*oFreq))
	if err != nil {
		panic(err)
	}
	fmt.Printf("%25s: %d\n", "SetFreq", *oFreq)

	err = airspy.SetLnaGain(dev, uint8(*oLnaGain))
	if err != nil {
		panic(err)
	}
	fmt.Printf("%25s: %d\n", "SetLnaGain", *oLnaGain)

	err = airspy.SetMixerGain(dev, uint8(*oMixerGain))
	if err != nil {
		panic(err)
	}
	fmt.Printf("%25s: %d\n", "SetMixerGain", *oMixerGain)

	err = airspy.SetVgaGain(dev, uint8(*oVgaGain))
	if err != nil {
		panic(err)
	}
	fmt.Printf("%25s: %d\n", "SetVgaGain", *oVgaGain)

	err = airspy.SetLinearityGain(dev, uint8(*oLinGain))
	if err != nil {
		panic(err)
	}
	fmt.Printf("%25s: %v\n", "SetLinearityGain", *oLinGain)

	err = airspy.SetSensitivityGain(dev, uint8(*oSensGain))
	if err != nil {
		panic(err)
	}
	fmt.Printf("%25s: %v\n", "SetSensitivityGain", *oSensGain)

	err = airspy.SetLnaAgc(dev, *oLnaAgc)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%25s: %v\n", "SetLnaAgc", *oLnaAgc)

	err = airspy.SetMixerAgc(dev, *oMixerAgc)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%25s: %v\n", "SetMixerAgc", *oMixerAgc)

	err = airspy.SetRfBias(dev, *oRfBias)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%25s: %v\n", "SetRfBias", *oRfBias)

	err = airspy.SetPacking(dev, *oPacking)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%25s: %v\n", "SetPacking", *oPacking)
}

func startRx(dev *airspy.Device) {

	var received uint64
	var dropped uint64

	// Starts reception thread
	last := time.Now()
	err := airspy.StartRx(dev, func(t *airspy.Transfer) int {

		received += uint64(t.SampleCount)
		dropped += t.DroppedSamples
		now := time.Now()
		if now.Sub(last) >= time.Second {
			fmt.Printf("Samples received: %d  Samples dropped: %d\n", received, dropped)
			last = now
		}
		return 0
	})
	if err != nil {
		panic(err)
	}
	fmt.Printf("\nReception started. Press Enter to exit\n")

	// Waits for user to press Enter
	data := make([]byte, 1)
	os.Stdin.Read(data)

	// Stops reception
	err = airspy.StopRx(dev)
	if err != nil {
		panic(err)
	}
}

// usage shows the application usage
func usage() {

	fmt.Fprintf(os.Stderr, "%s v%d.%d\n", ProgName, Vmajor, Vminor)
	fmt.Fprintf(os.Stderr, "usage: %s [options] \n", ExecName)
	flag.PrintDefaults()
	os.Exit(2)
}
